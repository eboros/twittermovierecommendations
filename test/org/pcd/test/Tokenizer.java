/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pcd.test;

import java.io.File;
import java.io.IOException;
import org.junit.Test;
import org.pcd.nlp.collection.FileTwitterStatusList;
import org.pcd.nlp.collection.TwitterStatusList;
import org.pcd.nlp.twitter.TokeniseMode;
import org.pcd.nlp.twitter.TwitterPreprocessingMode;
import org.pcd.nlp.twitter.TwitterStatus;
import org.pcd.utils.WatchedRunner;

/**
 *
 * @author Emanuela Boros
 */
public class Tokenizer {

    private static final String JSON_TWITTER = "/web/WEB-INF/json_tweets.txt";
    private String encoding = "UTF-8";
    private int nTweets = -1;
    private boolean veryLoud = true;
    long timeBeforeSkip = 0;

    @Test
    public void testTweetTokeniseJSON() throws IOException {
        String tokMode = "TOKENISE";

        final TokeniseMode mode = new TokeniseMode();
        long done = 0;
        long skipped = 0;
        long start = System.currentTimeMillis();

        TwitterStatusList twitterStatusList = getTwitterStatusList(new File(JSON_TWITTER));
        for (final TwitterStatus twitterStatus : twitterStatusList) {
            if (veryLoud) {
                System.out.println("\nPROCESSING TWEET");
                System.out.println(twitterStatus);
            }
            WatchedRunner runner = new WatchedRunner(getTimeBeforeSkip()) {

                @Override
                public void doTask() {
                    //for (TwitterPreprocessingMode<?> mode : modes) {
                    mode.process(twitterStatus);
                    //}
                }
            };
            runner.go();
            if (runner.taskCompleted()) {
                done++;
                System.out.println("\rDone: " + done);
                System.out.println(twitterStatus);
            } else {
                skipped++;
            }
            if (skipped > 0) {
                System.out.println(" (Skipped: " + skipped + ") ");
            }
        }

    }

    /**
     * @return the list of tweets from the input file
     * @throws IOException
     */
    public TwitterStatusList getTwitterStatusList(File inputFile) throws IOException {
        if (this.nTweets == -1) {
            return FileTwitterStatusList.read(inputFile, this.encoding);
        } else {
            return FileTwitterStatusList.read(inputFile, this.encoding, this.nTweets);
        }
    }

    private long getTimeBeforeSkip() {
        return this.timeBeforeSkip;
    }
}
