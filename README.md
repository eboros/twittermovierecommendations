# Movie Recommendations from Twitter Feed - GAE Application

This is a project thought to be a Google's cloud-based application that uses your tweets to 
create an image gallery with the most relevant movies that you'd want to see. 

# Data Gathering

It uses [Twitter4J](http://twitter4j.org/en/index.html):

``` java
    // Get a users tweets.
    private List<Status> getUserTimeline(String userName) {
        List<Status> statuses = new ArrayList<Status>();
        Twitter twitter = new TwitterFactory().getInstance();
        try {
            statuses = twitter.getUserTimeline(userName);
        } catch (TwitterException ex) {
            java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).
                log(Level.SEVERE, ex);
        }
        return statuses;
    }
```

# Tweet Processing

I am looking for Imdb references in tweets. Two notes:
* The links in the tweets are shortend.
* Difficult to extract the movie only having an Imdb link.

Mostly, the extraction of information from tweets is done with regex. I need only the links so this is 
a trivial problem. I would mention though the mmethod for unshortening the link.

``` java
    // Unshortening twitter links.
    public static String UNSHORT_REQ = "http://api.tweetmeme.com/url_info?url=";
    
    public static String getUnshortnedUrl(String unshortendUrl) {
        String response = "NOTSHORTEND";
        try {
            response = callMethod(UNSHORT_REQ + unshortendUrl);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).
                log(Level.SEVERE, ex.getMessage());
        }
        return response;
    }
    
    // The call made with URLFetch
    public static String callMethod(String endpoint) throws IOException {
        URL url = new URL(endpoint);
        URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
        HTTPResponse response = fetcher.fetch(url);
        return new String(response.getContent());
    }
```

# Movie Similarities

First of all, I need a dataset. Here it is: [MovieLens Data Sets] (http://www.grouplens.org/node/12)
I used the smallest one, with 1700 movies. It was more than satisfying for this use case.

I pre-processed the data and the final file has this form:

``` 
44|195|Terminator, The (1984)|5|http://us.imdb.com/M/title-exact?Terminator,%20The%20(1984)
264|200|Shining, The (1980)|5|http://us.imdb.com/M/title-exact?Shining,%20The%20(1980)
194|385|True Lies (1994)|2|http://us.imdb.com/M/title-exact?True%20Lies%20(1994)
72|195|Terminator, The (1984)|5|http://us.imdb.com/M/title-exact?Terminator,%20The%20(1984)
222|750|Amistad (1997)|5|http://us.imdb.com/M/title-exact?imdb-title-118607
```
The first column is the id of the movie, the second the id of the movie, the third is the rating and 
the forth is the Imdb url, which may become usefull.

Thanks to [Edwin Chen](http://twitter.com/echen), currently a data scientist at Twitter, who posted a Scala sample of how similarities between 
two movies can be performed, I managed to transfer the code to Java and to apply it successfully.

I needed:
* A pair of movies A and B and all the people who rated both A and B obtaining two vectors
* The total number of people who rated each movie
* The correlation between these two vectors 
(See the [Wikipedia page](http://en.wikipedia.org/wiki/Correlation_and_dependence) on correlation.)

Computing the **correlation** between two movies: 

``` scala
    public Double correlation(int size, Double dotProduct, Double ratingSum,
            Double rating2Sum, Double ratingNormSq, Double rating2NormSq) {

        double numerator = (Double) (size * dotProduct) - (Double) (ratingSum * rating2Sum);
        double denominator = Math.sqrt(size * ratingNormSq - ratingSum * ratingSum)
                * Math.sqrt(size * rating2NormSq - rating2Sum * rating2Sum);
        return (Double) (numerator / denominator);
    }
```

#The Gallery

There isn't much to say about this. Actually, in fact, the only thing I can say is that I use 
Flickr Photos Search API and get relevant images for movies, with tags like #cinema, #movie.

# Demo
[![Top Book-Crossing Pairs](https://dl-web.dropbox.com/get/Photos/New/twitterrec.png?w=a9eedca3)](https://dl-web.dropbox.com/get/Photos/New/twitterrec.png?w=a9eedca3)
[![Top Book-Crossing Pairs](https://dl-web.dropbox.com/get/Photos/New/twitterrec2.png?w=32d15068)](https://dl-web.dropbox.com/get/Photos/New/twitterrec2.png?w=32d15068)
