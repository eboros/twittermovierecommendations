var req;
var isIE;
var completeField;
var completeTable;
var autoRow;
var gallery;

function init() {
    completeField = document.getElementById("complete-field");
    completeTable = document.getElementById("complete-table");
    autoRow = document.getElementById("auto-row");
    gallery = document.getElementById("main");
    completeTable.style.top = getElementY(autoRow) + "px";
}

function doGetMovies() {

    var url = "media?twitterUserName=" + escape(completeField.value);
    var c = rjsSupport.get(url,'application/xml');
    parseMessages(c);
}

function initRequest() {
    if (window.XMLHttpRequest) {
        if (navigator.userAgent.indexOf('MSIE') != -1) {
            isIE = true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function callback() {
    clearTable();
    if (req.readyState == 4) {
        if (req.status == 200) {
            parseMessages(req.responseXML);
        }
    }
}

function appendMovieTo(title,url) {

    var li =document.createElement("li");
    var p = document.createElement("p");
    var img = document.createElement("img");
    img.setAttribute("src", url);
    p.appendChild(img);
    li.appendChild(p);
    
    var descP = document.createElement("p");
    descP.setAttribute("class", "desc");
    descP.appendChild(document.createTextNode(title));
    li.appendChild(descP);

    gallery.appendChild(li);
}

function clearTable() {
    if (completeTable.getElementsByTagName("tr").length > 0) {
        completeTable.style.display = 'none';
        for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
            completeTable.removeChild(completeTable.childNodes[loop]);
        }
    }
}

function getElementY(element){
    
    var targetTop = 0;
    
    if (element.offsetParent) {
        while (element.offsetParent) {
            targetTop += element.offsetTop;
            element = element.offsetParent;
        }
    } else if (element.y) {
        targetTop += element.y;
    }
    return targetTop;
}
function StringtoXML(text){
    if (window.ActiveXObject){
        var doc=new ActiveXObject('Microsoft.XMLDOM');
        doc.async='false';
        doc.loadXML(text);
    } else {
        var parser=new DOMParser();
        var doc=parser.parseFromString(text,'text/xml');
    }
    return doc;
}
function parseMessages(responseXML) {

    
    // no matches returned
    if (responseXML == null) {
        return false;
    } else {
        var doc = StringtoXML(responseXML);
        var movies = doc.getElementsByTagName("movies")[0];
        if (movies != null){
            if (movies.childNodes.length > 0) {
                completeTable.setAttribute("bordercolor", "black");
                completeTable.setAttribute("border", "1");
    
                for (loop = 0; loop < movies.childNodes.length; loop++) {
                    var movie = movies.childNodes[loop];
                    var title = movie.getElementsByTagName("title")[0];
                    var photos = movie.getElementsByTagName("photos")[0];
                    var photoChildren = photos.getElementsByTagName("photo");
                    for (i = 0; i < photoChildren.length; i++) {
                        var photo = photos.getElementsByTagName("photo")[i].childNodes[0].nodeValue;
                        appendMovieTo(title.childNodes[0].nodeValue, photo);
                    }
                }
            }
        } else {
            window.location = "/error.jsp"
        }
    }
}