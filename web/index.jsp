<%-- 
    Document   : index
    Created on : May 2, 2012, 7:02:15 PM
    Author     : Emanuela Boros
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
    ransitional//EN" "http://www.w3.org/
    TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.SimpleTimeZone" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="twitter4j.Status"%>
<%@ page import="org.pcd.collection.TwitterStatusList"%>
<%@ page import="org.pcd.sample.TwitterStatus"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="org.pcd.apis.MoviesParser.Movie"%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <title>Twitter Movie Recommendations</title>
        <link href='/style/skeleton.css' media='screen' rel='stylesheet' type='text/css' />
        <link href='/style/main.css' media='screen' rel='stylesheet' type='text/css' />
        <script language="JavaScript" type="text/JavaScript" src='/js/jquery.min.js'></script>
        <script language="JavaScript" type="text/JavaScript" src='/js/javascript.js'></script>
        <script language="JavaScript" type="text/JavaScript" src='/js/Support.js'></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
        <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    </head>
    <body onload="init()">

        <div class="container">
            <h2>Twitter Movie Recommendations</h2>

            <div class="sixteen columns">
                <div class="section clearfix" id="clients">

                    <!-- <form name="autofillform" action="media">-->
                    <table border="0" cellpadding="5">
                        <tbody>
                            <tr><td></td>
                        <ul>
                            <li style="text-align: center">
                                <p class="author">@ <input type="text" id="complete-field"/></p>
                            </li>
                        </ul>
                        <ul>
                            <li style="text-align: center">
                                <button onclick="doGetMovies()">Find Movies</button>
                            </li>
                        </ul>
                        <tr>
                            <td></td>
                        </tr>
                        </tr>
                        <tr>
                            <td id="auto-row" colspan="2">
                                <table id="complete-table" class="popupBox" />
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!--</form>-->
                    <div id="gallery">
                        <ul id="main"></ul>
                    </div>
                </div>
            </div>
          
    </body>


</html>
