package org.pcd.sample.nlp;

/**
 *
 * @author Emanuela Boros
 */
public class LevenshteinDistance {

    class TextNormalizer {

        public String normalize(String s) {
            String normString = s.toLowerCase();
            normString = normString.replaceAll("-", "");
            normString = normString.replaceAll("_", "");

            return normString;
        }
    }
    TextNormalizer tn = new TextNormalizer();

    public double getScore(Object o1, Object o2) {

        String text1 = (String) o1;
        String text2 = (String) o2;
        text1 = tn.normalize(text1);
        text2 = tn.normalize(text2);
        if ((text1.length() < 3) || (text2.length() < 3)) {
            return 0;
        }
        int minlength = Math.min(text1.length(), text2.length());
        double demLevDistance = damerauLevenshteinDistance(text1, text2);
        double synSim = (minlength - demLevDistance) / minlength;
        synSim = Math.min(synSim, 1);
        synSim = Math.max(synSim, 0);
        return synSim;
    }

    public double getSimilarity(String text1, String text2) {

        if ((text1.length() < 3) || (text2.length() < 3)) {
            return 0;
        }
        int minlength = Math.min(text1.length(), text2.length());
        double demLevDistance = damerauLevenshteinDistance(text1, text2);
        double synSim = (minlength - demLevDistance) / minlength;
        synSim = Math.min(synSim, 1);
        synSim = Math.max(synSim, 0);
        return synSim;
    }

    private int damerauLevenshteinDistance(String str1, String str2) {

        int l1 = str1.length();
        int l2 = str2.length();
        int i;
        int j;
        int cost;
        int d[][] = new int[l1 + 1][l2 + 1];

        for (i = 0; i <= l1; i++) {
            d[i][0] = i;
        }
        for (j = 0; j <= l2; j++) {
            d[0][j] = j;
        }

        for (i = 1; i <= l1; i++) {
            for (j = 1; j <= l2; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    cost = 0;
                } else {
                    cost = 1;
                }
                d[i][j] = Math.min(d[i - 1][j] + 1, Math.min(d[i][j - 1] + 1, d[i - 1][j - 1] + cost));
                if ((i > 1) && (j > 1) && (str1.charAt(i - 1) == str2.charAt(j - 2))
                        && (str1.charAt(i - 2) == str2.charAt(j - 1))) {
                    d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
                }
            }
        }
        return d[l1][l2];
    }
}
