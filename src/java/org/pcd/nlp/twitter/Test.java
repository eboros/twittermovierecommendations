/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pcd.nlp.twitter;

import com.google.appengine.repackaged.org.json.JSONArray;
import com.google.appengine.repackaged.org.json.JSONException;
import com.google.appengine.repackaged.org.json.JSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.pcd.nlp.collection.FileTwitterStatusList;
import org.pcd.nlp.collection.TwitterStatusList;
import org.pcd.nlp.twitter.StemmingMode;
import org.pcd.nlp.twitter.TokeniseMode;
import org.pcd.nlp.twitter.TwitterStatus;
import org.pcd.nlp.patterns.EdgePunctuationPatternProvider;
import org.pcd.nlp.patterns.URLPatternProvider;
import org.pcd.utils.WatchedRunner;

/**
 *
 * @author Emanuela Boros
 */
public class Test {

    private static final String JSON_TWITTER = "/resources/json_tweets.txt";
    private static String encoding = "UTF-8";
    private static int nTweets = -1;
    private static boolean veryLoud = true;
    private static long timeBeforeSkip = 0;

    public static void main(String[] sdfsd) throws JSONException {
        try {
            try {
                testPatterns();
            } catch (URISyntaxException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void tokenizingTest() throws IOException {

        URL url = Test.class.getResource(JSON_TWITTER);
        System.out.println(url);
        String source = IOUtils.toString(url.openStream());
        try {
            String tokMode = "TOKENISE";
            final TokeniseMode mode = new TokeniseMode();
            long done = 0;
            long skipped = 0;
            long start = System.currentTimeMillis();
            TwitterStatusList twitterStatusList = getTwitterStatusList(new File(url.toURI()));
            for (final TwitterStatus twitterStatus : twitterStatusList) {
                if (veryLoud) {
                    System.out.println("\nPROCESSING TWEET");
                    System.out.println(twitterStatus);
                }
                WatchedRunner runner = new WatchedRunner(getTimeBeforeSkip()) {

                    @Override
                    public void doTask() {
                        Map<String, List<String>> results = mode.process(twitterStatus);
                        for (Map.Entry<String, List<String>> entry : results.entrySet()) {
                            System.out.println("-" + entry.getKey() + "-");
                            for (String element : entry.getValue()) {
                                System.out.println("  -" + element);
                            }
                        }
                    }
                };
                runner.go();
                if (runner.taskCompleted()) {
                    done++;
                    System.out.println("\rDone: " + done);
                    System.out.println(twitterStatus);
                } else {
                    skipped++;
                }
                if (skipped > 0) {
                    System.out.println(" (Skipped: " + skipped + ") ");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void stemmngTest() throws IOException {
        URL url = Test.class.getResource(JSON_TWITTER);
        System.out.println(url);
        String source = IOUtils.toString(url.openStream());
        try {
            String tokMode = "TOKENISE";
            final StemmingMode mode = new StemmingMode();
            long done = 0;
            long skipped = 0;
            long start = System.currentTimeMillis();
            TwitterStatusList twitterStatusList = getTwitterStatusList(new File(url.toURI()));
            for (final TwitterStatus twitterStatus : twitterStatusList) {
                if (veryLoud) {
                    System.out.println("\nPROCESSING TWEET");
                    System.out.println(twitterStatus);
                }
                WatchedRunner runner = new WatchedRunner(getTimeBeforeSkip()) {

                    @Override
                    public void doTask() {
                        List<String> results = mode.process(twitterStatus);
                        for (String element : results) {
                            System.out.println("  -" + element);
                        }
                    }
                };
                runner.go();
                if (runner.taskCompleted()) {
                    done++;
                    System.out.println("\rDone: " + done);
                    System.out.println(twitterStatus);
                } else {
                    skipped++;
                }
                if (skipped > 0) {
                    System.out.println(" (Skipped: " + skipped + ") ");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testPatterns() throws IOException, URISyntaxException, JSONException {
        URL url = Test.class.getResource(JSON_TWITTER);
        System.out.println(url);
        String source = IOUtils.toString(url.openStream());
        URLPatternProvider.DFURLPatternProvider provider = new URLPatternProvider.DFURLPatternProvider();
        Pattern p = provider.pattern();
        TwitterStatusList twitterStatusList = getTwitterStatusList(new File(url.toURI()));
        for (final TwitterStatus twitterStatus : twitterStatusList) {
            if (veryLoud) {
                System.out.println("\nPROCESSING TWEET");
                System.out.println("[" + twitterStatus + "]");
            }
            String tweet = EdgePunctuationPatternProvider.fixedges(twitterStatus.text);
            Matcher matches = p.matcher(tweet);
            while (matches.find()) {
                String szUrl = tweet.substring(matches.start(), matches.end());
                System.out.println("URL: " + szUrl);
                if (szUrl.contains("imdb")) {
                    System.out.println(getImdbId(tweet.substring(matches.start(), matches.end())));
                    String response =
                            callMethod(REQUEST + getImdbId(tweet.substring(matches.start(), matches.end())));

                    JsonParser parser = new JsonParser();
                    JsonElement element = parser.parse(response);

                    System.out.println(element);
                    //JSONObject jsonObj = new JSONObject(element);
                    JsonObject jsonObj = element.getAsJsonObject();
                    System.out.println(jsonObj.get("title"));
//                JSONArray jsonArry  = jsonObj.getJSONArray(element.toString());
//                for(int i = 0; i < jsonArry.length(); i++) {
//			System.out.println(jsonArry.get(i));
//		}
                }
            }
        }
    }
    private static final String IMDB_ID = "tt([0-9]+)";
    private static String REQUEST = "http://www.deanclatworthy.com/imdb/?id=";

    public static Pattern pattern(String patternString) {
        return Pattern.compile(patternString, Pattern.UNICODE_CASE
                | Pattern.CASE_INSENSITIVE);
    }

    public static String getImdbId(String szUrl) {
        String imdbId = "";
        Pattern p = pattern(IMDB_ID);
        if (szUrl.contains("imdb")) {
            Matcher matches = p.matcher(szUrl);
            while (matches.find()) {
                imdbId = szUrl.substring(matches.start(), matches.end());
            }
        }
        return imdbId;
    }

    /**
     * @return the list of tweets from the input file
     * @throws IOException
     */
    public static TwitterStatusList getTwitterStatusList(File inputFile) throws IOException {
        if (nTweets == -1) {
            return FileTwitterStatusList.read(inputFile, encoding);
        } else {
            return FileTwitterStatusList.read(inputFile, encoding, nTweets);
        }
    }

    public static String callMethod(String ENDPOINT) throws IOException {
        URL url = new URL(ENDPOINT);
        URLConnection connection = url.openConnection();
        InputStream in = connection.getInputStream();
        BufferedReader res = new BufferedReader(new InputStreamReader(in, "UTF-8"));

        StringBuilder sBuffer = new StringBuilder();
        String inputLine;
        while ((inputLine = res.readLine()) != null) {
            sBuffer.append(inputLine);
        }

        res.close();

        return sBuffer.toString();
    }

    private static long getTimeBeforeSkip() {
        return timeBeforeSkip;
    }
}
