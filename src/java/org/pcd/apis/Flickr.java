package org.pcd.apis;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Emanuela Boros
 */
public class Flickr {

    public class ArtistPhoto {

        private String source;

        public ArtistPhoto(String source) {
            this.source = source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getSource() {
            return source;
        }
    }

    /**
     * Returns photos in ArtistPhoto format
     *
     * @param searchText
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public List<ArtistPhoto> getPhotos(String searchText)
            throws MalformedURLException, IOException {

        System.out.println("Looking on Flickr for " + searchText);
        ArrayList<ArtistPhoto> photoURLs = new ArrayList<ArtistPhoto>();

        String url = "http://api.flickr.com/services/rest/?method=flickr.photos.search&"
                + "api_key=85dbc2834022e437cdd73a2de1786876&per_page=5&tags=movie%2Cposter%2Ccinema%2Cfilm&text=";
        url += searchText.toLowerCase().replaceAll(" ", "_");
        URLConnection uc = new URL(url).openConnection();
        DataInputStream dis = new DataInputStream(uc.getInputStream());

        XPathFactory factory = XPathFactory.newInstance();

        XPath xpath = factory.newXPath();

        try {
            InputSource inputXml = new InputSource(dis);

            NodeList nodes = (NodeList) xpath.evaluate("/rsp/photos/photo",
                    inputXml, XPathConstants.NODESET);

            for (int i = 0, n = nodes.getLength(); i < n; i++) {

                NamedNodeMap nodeAttributes = nodes.item(i).getAttributes();
                String farm = nodeAttributes.getNamedItem("farm").getTextContent();
                String id = nodeAttributes.getNamedItem("id").getTextContent();
                String server = nodeAttributes.getNamedItem("server").getTextContent();
                String secret = nodeAttributes.getNamedItem("secret").getTextContent();

                String flickrurl = "http://" + "farm" + farm
                        + ".static.flickr.com/";
                flickrurl += server + "/" + id + "_" + secret + ".jpg";

                photoURLs.add(new ArtistPhoto(flickrurl));
                System.out.println("Found " + flickrurl);
            }
        } catch (XPathExpressionException ex) {
            System.out.print("XPath Error");
            System.out.print("File Error");
        }

        return photoURLs;
    }
}
