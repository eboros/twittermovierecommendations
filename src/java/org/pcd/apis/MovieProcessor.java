/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pcd.apis;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pcd.apis.MoviesParser.Movie;
import org.pcd.sample.nlp.LevenshteinDistance;

/**
 *
 * @author Emanuela Boros
 */
public class MovieProcessor {

    private static final List<Movie> MOVIES = new ArrayList<Movie>();
    private static final String MOVIE_RATINGS = "/resources/ratings.csv";

    public void initialize() throws IOException {
        this.fillAllMovies();
    }

    public void fillAllMovies() throws IOException {
        Logger.getLogger(MovieProcessor.class.getName()).log(Level.INFO, "Initializing Movie Database");
        InputStream fstream = MoviesParser.class.getResourceAsStream(MOVIE_RATINGS);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        while ((strLine = br.readLine()) != null) {
            String[] strips = strLine.split("[|]");
            String userid = strips[0];
            String movieid = strips[1];
            String title = strips[2];
            String rating = strips[3];
            String url = strips[4];
            Movie movie = new Movie(movieid, title, url);
            boolean found = false;
            for (Movie m : MOVIES) {
                if (m.id.equals(movieid)) {
                    movie = m;
                    found = true;
                }
            }
            if (!found) {
                MOVIES.add(movie);
            }
            movie.ratings.add(Integer.parseInt(rating));
            movie.userids.add(userid);
            //System.out.println(Arrays.toString(strips));
        }
        in.close();
        Logger.getLogger(MovieProcessor.class.getName()).log(Level.INFO, MOVIES.size() + " movies");
    }

    public String getMovieUrl(String movieId) throws IOException {
        String noUrl = "";
        //for (Map.Entry<Movie, Integer> entry : movies.entrySet()) {
        //     if (((Movie) entry.getKey()).id.equals(movieId)) {
        for (Movie movie : MOVIES) {
            if (movie.id.equals(movieId)) {
                return movie.url;
            }
        }
        //     }
        // }
        return noUrl;
    }

    public String getMovieTitle(String movieId) throws IOException {
        String notitle = "";
        //for (Map.Entry<Movie, Integer> entry : movies.entrySet()) {
        //    if (((Movie) entry.getKey()).id.equals(movieId)) {
        for (Movie movie : MOVIES) {
            if (movie.id.equals(movieId)) {
                return movie.title;
            }
        }
        //     }
        //}
        return notitle;
    }

    public Movie doYouHaveTheMovie(String title) {
        double finalScore = Double.MIN_VALUE;
        Movie foundMovie = new Movie("noid");
        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        for (Movie movie : MOVIES) {
            double score = levenshteinDistance.getScore(title, movie.title);
            if (score > finalScore) {
                finalScore = score;
                foundMovie = movie;
            }
        }
        if (!foundMovie.id.equals("noid")) {
            return foundMovie;
        }
        return null;
    }

    public Map<Movie, Double> getCorrelations(Movie movie, int count) {
        Map<Movie, Double> correlations = new LinkedHashMap<Movie, Double>();
        for (Movie movie2 : MOVIES) {
            Double corr = computeCorrelation(movie, movie2);
            if (!corr.isNaN()) {
                if (corr > 0d) {
                    correlations.put(movie2, corr);
                }
            }
        }
        return correlations;
    }

    private Double computeCorrelation(Movie movie1, Movie movie2) {
        double dotProduct = 0d;
        double ratingSum = 0d;
        double rating2Sum = 0d;
        double ratingNormSq = 0d;
        double rating2NormSq = 0d;

        int size = 0;
        for (int i = 0; i < movie1.userids.size(); i++) {
            String userid = movie1.userids.get(i);
            if (movie2.userids.contains(userid)) {
                double ratingProd = 0d;
                double ratingSq = 0d;
                double rating2Sq = 0d;
                size++;
                int j = movie2.userids.indexOf(userid);

                ratingProd = movie1.ratings.get(i) * movie2.ratings.get(j);
                ratingSq = movie1.ratings.get(i) * movie1.ratings.get(i);
                rating2Sq = movie2.ratings.get(j) * movie2.ratings.get(j);

                dotProduct += ratingProd;
                ratingSum += movie1.ratings.get(i);
                rating2Sum += movie2.ratings.get(j);
                ratingNormSq += ratingSq;
                rating2NormSq += rating2Sq;
            }
        }
        return correlation(size, dotProduct, ratingSum,
                rating2Sum, ratingNormSq, rating2NormSq);
    }

    public Double correlation(int size, Double dotProduct, Double ratingSum,
            Double rating2Sum, Double ratingNormSq, Double rating2NormSq) {

        double numerator = (Double) (size * dotProduct) - (Double) (ratingSum * rating2Sum);
        double denominator = Math.sqrt(size * ratingNormSq - ratingSum * ratingSum)
                * Math.sqrt(size * rating2NormSq - rating2Sum * rating2Sum);
        return (Double) (numerator / denominator);
    }
}
