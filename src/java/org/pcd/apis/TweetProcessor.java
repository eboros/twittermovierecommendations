/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pcd.apis;

import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import java.util.List;
import java.util.logging.Level;
import org.pcd.nlp.collection.MemoryTwitterStatusList;
import org.pcd.nlp.collection.TwitterStatusList;
import org.pcd.nlp.twitter.TwitterStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sup.social.media.SupTwitterClient;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.pcd.nlp.patterns.EdgePunctuationPatternProvider;
import org.pcd.nlp.patterns.URLPatternProvider;

/**
 *
 * @author Emanuela Boros
 */
public class TweetProcessor {

    private static final Logger log =
            LoggerFactory.getLogger(TweetProcessor.class);
    private static final String IMDB_ID = "tt([0-9]+)";
    public static String IMDB_REQ = "http://www.deanclatworthy.com/imdb/?id=";
    public static String UNSHORT_REQ = "http://api.unshorten.it?shortURL=NOTSHORT&responseFormat=text&"
            + "apiKey=1f121a6fc59bda8f9d4e9c2fff6bf89d";
    public static String UNSHORT_REQ2 = "http://api.tweetmeme.com/url_info?url=";

    public static Pattern pattern(String patternString) {
        return Pattern.compile(patternString, Pattern.UNICODE_CASE
                | Pattern.CASE_INSENSITIVE);
    }

    public TwitterStatusList getTweets(String twitterUsername) {
        TwitterStatusList twitterStatusList = new MemoryTwitterStatusList();
        List<Status> timeline = getUserTimeline(twitterUsername);
        for (Status twitterStatus : timeline) {
            twitterStatusList.add(TwitterStatus.fromAPI(twitterStatus));
        }
//        CursoredList<TwitterProfile> friendsProfiles = twitterClient.getFollowers();
//        for (TwitterProfile friendsProfile : friendsProfiles) {
//            //log.info(friendsProfile.getScreenName() + ", " + friendsProfile.getName());
//            try {
//                List<Status> friendTimeline = getUserTimeline(friendsProfile.getScreenName());
//                for (Status twitterStatus : friendTimeline) {
//                    twitterStatusList.add(TwitterStatus.fromAPI(twitterStatus));
//                }
//            } catch (TwitterException ex) {
//                log.error(ex.getMessage());
//            }
//        }
        return twitterStatusList;
    }

    public TwitterStatusList getTweets(String twitterUsername, int start, int end) {
        System.out.println("Tweets for " + twitterUsername);
        TwitterStatusList twitterStatusList = new MemoryTwitterStatusList();
        List<Status> timeline = getUserTimeline(twitterUsername);
        for (Status twitterStatus : timeline) {
            twitterStatusList.add(TwitterStatus.fromAPI(twitterStatus));
        }
//        CursoredList<TwitterProfile> friendsProfiles = twitterClient.getFollowers();
//        for (TwitterProfile friendsProfile : friendsProfiles) {
//            //log.info(friendsProfile.getScreenName() + ", " + friendsProfile.getName());
//            try {
//                List<Status> friendTimeline = getUserTimeline(friendsProfile.getScreenName());
//                for (Status twitterStatus : friendTimeline) {
//                    twitterStatusList.add(TwitterStatus.fromAPI(twitterStatus));
//                }
//            } catch (TwitterException ex) {
//                log.error(ex.getMessage());
//            }
//        }
        return twitterStatusList;
    }

    public TwitterStatusList getRelevantTweets(String twitterUsername, int start, int end)
            throws IOException {

        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO, "getRelevantTweets");
        TwitterStatusList twitterStatusList = getTweets(twitterUsername);
        TwitterStatusList relevantTweets = new MemoryTwitterStatusList();

        for (final TwitterStatus twitterStatus : twitterStatusList) {
            String tweet = EdgePunctuationPatternProvider.fixedges(twitterStatus.text);
            Matcher matches = p.matcher(tweet);
            while (matches.find()) {
                String szUrl = tweet.substring(matches.start(), matches.end());
                String szUnshortnedUrl = getUnshortnedUrl(szUrl);
                if (szUnshortnedUrl.contains("imdb")) {
                    relevantTweets.add(twitterStatus);
                }
            }
        }
        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO,
                "Finishing getRelevantTweets");
        return relevantTweets;
    }

    public List<String> getMovies(String twitterUsername) throws IOException {

        List<String> movies = new ArrayList<String>();

        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO, "getMovies");
        TwitterStatusList twitterStatusList = getTweets(twitterUsername);
        TwitterStatusList relevantTweets = new MemoryTwitterStatusList();

        for (final TwitterStatus twitterStatus : twitterStatusList) {
            String tweet = EdgePunctuationPatternProvider.fixedges(twitterStatus.text);
            Matcher matches = p.matcher(tweet);
            while (matches.find()) {
                String szUrl = tweet.substring(matches.start(), matches.end());
                String szUnshortnedUrl = getUnshortnedUrl(szUrl);
                if (szUnshortnedUrl.contains("imdb")) {
                    String response =
                            callMethod(IMDB_REQ + getImdbId(szUnshortnedUrl));
                    relevantTweets.add(twitterStatus);
                    JsonObject jsonObj = new JsonParser().parse(response).getAsJsonObject();
                    movies.add(jsonObj.get("title").toString());
                }
            }
        }
        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO, "Finishing getMovies");
        return movies;
    }
    static final URLPatternProvider.DFURLPatternProvider provider =
            new URLPatternProvider.DFURLPatternProvider();
    static Pattern p = provider.pattern();

    public List<String> getMovies(String twitterUsername, int start, int end)
            throws IOException {

        List<String> movies = new ArrayList<String>();

        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO, "getMovies");

        TwitterStatusList twitterStatusList = getTweets(twitterUsername, start, end);

        for (final TwitterStatus twitterStatus : twitterStatusList) {
            String tweet = twitterStatus.text;
            Matcher matches = p.matcher(tweet);
            while (matches.find()) {
                String szUrl = tweet.substring(matches.start(), matches.end());
                String szUnshortnedUrl = getUnshortnedUrl(szUrl);
                if (szUnshortnedUrl.contains("imdb")) {
                    String response =
                            callMethod(IMDB_REQ + getImdbId(szUnshortnedUrl));
                    JsonObject jsonObj = new JsonParser().parse(response).getAsJsonObject();
                    movies.add(jsonObj.get("title").toString());
                    System.out.println(jsonObj.get("title").toString());
                }
            }
        }
        java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.INFO, "Finishing getMovies");
        return movies;
    }

    public static String getImdbId(String szUrl) {
        String imdbId = "";
        Pattern pattern = pattern(IMDB_ID);
        if (szUrl.contains("imdb")) {
            Matcher matches = pattern.matcher(szUrl);
            while (matches.find()) {
                imdbId = szUrl.substring(matches.start(), matches.end());
            }
        }
        return imdbId;
    }

    public static String getUnshortnedUrl(String szUrl) {
        String response = "NOTSHORT";
        try {
            response = callMethod(UNSHORT_REQ2 + szUrl);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return response;
    }

    public static String callMethod(String ENDPOINT) throws IOException {

        URL url = new URL(ENDPOINT);
        URLFetchService fetcher = URLFetchServiceFactory.getURLFetchService();
        HTTPResponse response = fetcher.fetch(url);
        return new String(response.getContent());
    }

    private List<Status> getUserTimeline(String userName) {
        List<Status> statuses = new ArrayList<Status>();
        Twitter twitter = new TwitterFactory().getInstance();
        try {
            statuses = twitter.getUserTimeline(userName);
        } catch (TwitterException ex) {
            java.util.logging.Logger.getLogger(TweetProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statuses;
    }
}
