package org.pcd.apis;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pcd.apis.MoviesParser.Movie;
import org.pcd.nlp.collection.TwitterStatusList;

/**
 *
 * @author Emanuela Boros
 */
public class BackendWorker {

    private static BackendWorker backendWorker;
    private MovieProcessor movieProcessor;
    private TweetProcessor tweetProcessor;

    public static BackendWorker getInstance() {
        Logger.getLogger(BackendWorker.class.getName()).log(Level.INFO,
                "Initializing " + BackendWorker.class.getName());
        if (backendWorker == null) {
            backendWorker = new BackendWorker();
            backendWorker.movieProcessor = new MovieProcessor();
            backendWorker.tweetProcessor = new TweetProcessor();
            try {
                backendWorker.movieProcessor.initialize();
            } catch (IOException ex) {
                Logger.getLogger(BackendWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return backendWorker;
    }

    public TwitterStatusList getRelevantTweets(String twitterUserName, int start, int end) {
        try {
            TwitterStatusList twitterRelevantStatusList =
                    tweetProcessor.getRelevantTweets(twitterUserName, start, end);
            return twitterRelevantStatusList;
        } catch (IOException ex) {
            Logger.getLogger(BackendWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<String> getMovies(String twitterUserName) {
        try {
            List<String> movies = tweetProcessor.getMovies(twitterUserName);
            return movies;
        } catch (IOException ex) {
            Logger.getLogger(BackendWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<String> getMovies(String twitterUserName, int start, int end) {
        try {
            List<String> movies = tweetProcessor.getMovies(twitterUserName, start, end);
            return movies;
        } catch (IOException ex) {
            Logger.getLogger(BackendWorker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Map<Movie, Double> getCorrelatedMovies(String title, int count) {
        Movie foundMovie = movieProcessor.doYouHaveTheMovie(title);
        if (foundMovie == null) {
            return new HashMap<Movie, Double>();
        }
        return movieProcessor.getCorrelations(foundMovie, count);
    }
}
