/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pcd.apis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Emanuela Boros
 */
public class MoviesParser {
// Merge `ratings` with `numRaters`, by joining on their movie fields.

    public static class Movie implements Comparable<Movie> {

        public Movie(String id) {
            this.id = id;
        }

        public Movie(String id, String title, String url) {
            this.id = id;
            this.title = title;
            this.url = url;
        }
        public String title;
        public String id;
        public String url;
        public List<String> userids = new ArrayList<String>();
        public List<Integer> ratings = new ArrayList<Integer>();

        @Override
        public String toString() {
            return id + "," + title + "," + url;
        }

        public int compareTo(Movie o) {
            if (Integer.parseInt(id) == Integer.parseInt(o.id)) {
                return 0;
            }
            return -1;
        }
    }
    private static Map<Movie, Integer> movies = new HashMap<Movie, Integer>();
    private static final String JSON_TWITTER = "/resources/u.item";
    private static final String RATINGS = "/resources/u.data";
    private static final String RATINGS_OUT = "/resources/ratings.csv";
    private static final List<Movie> moviesFinal = new ArrayList<Movie>();

    public static void main(String[] sdf) throws URISyntaxException {
        try {
            fillAllMovies();
            testCorrelation();
            //createRatings();
        } catch (IOException ex) {
            Logger.getLogger(MoviesParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void createRatings() throws IOException, URISyntaxException {
        try {
            InputStream fstream = MoviesParser.class.getResourceAsStream(JSON_TWITTER);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] strips = strLine.split("[|]");
                System.out.println(strips[1] + "," + strips[4]);
                Movie movie = new Movie(strips[0], strips[1], strips[4]);
                movies.put(movie, 0);
                //System.out.println(Arrays.toString(strips));
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
        readRatings();
    }

    public static void readRatings() throws IOException, URISyntaxException {
        URL url = MoviesParser.class.getResource(RATINGS_OUT);
        File file = new File(url.toURI());
        System.out.println(file.getAbsolutePath());
        Writer outstream = new FileWriter(file);
        //OutputStream fstream = MoviesParser.class.getResourceAsStream(RATINGS_OUT);
        BufferedWriter out = new BufferedWriter(outstream);
        try {
            InputStream fstream = MoviesParser.class.getResourceAsStream(RATINGS);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] strips = strLine.split("(,)");
                String user = strips[0];
                String movieid = strips[1];
                String rating = strips[2];

//                System.out.println(user + "," + movieid + "," + getMovieTitle(movieid) + ","
//                        + rating + "," + getMovieUrl(movieid));
                out.write(user + "|" + movieid + "|" + getMovieTitle(movieid) + "|"
                        + rating + "|" + getMovieUrl(movieid) + "\n");
                out.flush();
            }
            in.close();
            out.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static String getMovieTitle(String movieid) throws IOException {
        Movie movie = new Movie(movieid);
        String notitle = "";
        for (Map.Entry<Movie, Integer> entry : movies.entrySet()) {
            if (((Movie) entry.getKey()).id.equals(movieid)) {
                return ((Movie) entry.getKey()).title;
            }
        }
        return notitle;
    }

    public static String getMovieUrl(String movieid) throws IOException {
        Movie movie = new Movie(movieid);
        String nourl = "";
        for (Map.Entry<Movie, Integer> entry : movies.entrySet()) {
            if (((Movie) entry.getKey()).id.equals(movieid)) {
                return ((Movie) entry.getKey()).url;
            }
        }
        return nourl;
    }

    public static void fillAllMovies() throws IOException {

        InputStream fstream = MoviesParser.class.getResourceAsStream(RATINGS_OUT);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        while ((strLine = br.readLine()) != null) {
            String[] strips = strLine.split("[|]");
            String userid = strips[0];
            String movieid = strips[1];
            String title = strips[2];
            String rating = strips[3];
            String url = strips[4];
            Movie movie = new Movie(movieid, title, url);
            boolean found = false;
            for (Movie m : moviesFinal) {
                if (m.id.equals(movieid)) {
                    movie = m;
                    found = true;
                }

            }
            if (!found) {
                moviesFinal.add(movie);
            }
            movie.ratings.add(Integer.parseInt(rating));
            movie.userids.add(userid);
            //System.out.println(Arrays.toString(strips));
        }
        in.close();

//        for (Movie m : moviesFinal) {
//            System.out.println("--" + m.title + "," + m.id);
//            for (int i : m.ratings) {
//                System.out.print(i + ",");
//            }
//            System.out.println();
//            for (String user : m.userids) {
//                System.out.print(user + ",");
//            }
//            System.out.println();
//        }
        System.out.println(moviesFinal.size() + " movies");
    }

    private static void testCorrelation() {

        int START = 0;
        int END = moviesFinal.size();
        Random random = new Random();

        Movie movie1 = moviesFinal.get(showRandomInteger(START, END, random));
        //Movie movie2 = moviesFinal.get(showRandomInteger(START, END, random));

        for (Movie movie2 : moviesFinal) {
            Double corr = computeCorrelation(movie1, movie2);
            if (!corr.isNaN()) {
                System.out.println(movie1.title + "/" + movie2.title+ ": "+corr);
            }
        }
    }

    private static int showRandomInteger(int aStart, int aEnd, Random aRandom) {
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long) aEnd - (long) aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
        return (int) (fraction + aStart);
    }

    private static Double computeCorrelation(Movie movie1, Movie movie2) {

        double dotProduct = 0d;
        double ratingSum = 0d;
        double rating2Sum = 0d;
        double ratingNormSq = 0d;
        double rating2NormSq = 0d;

        int size = 0;
        for (int i = 0; i < movie1.userids.size(); i++) {
            String userid = movie1.userids.get(i);
            if (movie2.userids.contains(userid)) {
                double ratingProd = 0d;
                double ratingSq = 0d;
                double rating2Sq = 0d;
                size++;
                int j = movie2.userids.indexOf(userid);
//                System.out.println("Movie1 " + userid + ":" + movie1.ratings.get(i)
//                        + "/" + " Movie2 " + userid + ":" + movie2.ratings.get(j));

                ratingProd = movie1.ratings.get(i) * movie2.ratings.get(j);
                ratingSq = movie1.ratings.get(i) * movie1.ratings.get(i);
                rating2Sq = movie2.ratings.get(j) * movie2.ratings.get(j);

                dotProduct += ratingProd;
                ratingSum += movie1.ratings.get(i);
                rating2Sum += movie2.ratings.get(j);
                ratingNormSq += ratingSq;
                rating2NormSq += rating2Sq;
            }
        }
//        System.out.println("Correlation: " + correlation(size, dotProduct, ratingSum,
//                rating2Sum, ratingNormSq, rating2NormSq));

        return correlation(size, dotProduct, ratingSum,
                rating2Sum, ratingNormSq, rating2NormSq);
    }

    public static Double correlation(int size, Double dotProduct, Double ratingSum,
            Double rating2Sum, Double ratingNormSq, Double rating2NormSq) {

        double numerator = (Double) (size * dotProduct) - (Double) (ratingSum * rating2Sum);
        double denominator = Math.sqrt(size * ratingNormSq - ratingSum * ratingSum)
                * Math.sqrt(size * rating2NormSq - rating2Sum * rating2Sum);
        return (Double) (numerator / denominator);
    }
}
