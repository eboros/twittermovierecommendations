package org.pcd.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javax.servlet.RequestDispatcher;
import org.pcd.apis.BackendWorker;
import org.pcd.apis.MoviesParser.Movie;
import org.pcd.apis.Flickr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nbuser
 */
public class MoviesInformationServlet extends HttpServlet {

    private ServletContext context;
    private static final Logger log =
            LoggerFactory.getLogger(MoviesInformationServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.context = config.getServletContext();
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        StringBuilder sb = new StringBuilder();

        String twitterUsername = request.getParameter("twitterUserName");
        Key userKey = KeyFactory.createKey(twitterUsername, twitterUsername);
        Date date = new Date();
        System.out.println("Showing @" + twitterUsername);
        List<String> movies = BackendWorker.getInstance().getMovies(twitterUsername, 0, 5);
        Map<Movie, Double> correlatedMovies = new LinkedHashMap<Movie, Double>();
        for (String title : movies) {
            correlatedMovies.putAll(BackendWorker.getInstance().getCorrelatedMovies(title, 5));
        }
        List<Movie> keySet = new ArrayList<Movie>(correlatedMovies.keySet());
        List<Double> mapValues = new ArrayList<Double>(correlatedMovies.values());
        TreeSet<Double> sortedSet = new TreeSet<Double>(mapValues);
        Object[] sortedArray = sortedSet.toArray();
        int size = sortedArray.length;
        if (size <= 1) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {

            Map<Movie, Double> map = new LinkedHashMap<Movie, Double>();
            System.out.println(size);
            for (int i = size - 1; i > size - 6; i--) {
                if (i < sortedArray.length) {
                    map.put(keySet.get(mapValues.indexOf(sortedArray[i])),
                            (Double) sortedArray[i]);
                }
            }
            for (Map.Entry<Movie, Double> entry : map.entrySet()) {
                System.out.println(entry.getKey().title
                        + ": " + entry.getValue());
            }
            System.out.println("Found Movies " + correlatedMovies.size());

//            Entity twitterUser = new Entity("TwitterUser", userKey);
//            twitterUser.setProperty("user", twitterUsername);
//            twitterUser.setProperty("date", date);
//            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
//            datastore.put(twitterUser);
//            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS");
//            fmt.setTimeZone(new SimpleTimeZone(0, ""));

            boolean moviesAdded = false;
            Flickr flickr = new Flickr();

            int i = 0;
            DecimalFormat df = new DecimalFormat("#.##");
            for (Map.Entry<Movie, Double> entry : map.entrySet()) {
                if (i < 5) {
                    String title = ((Movie) entry.getKey()).title;
                    double corr = entry.getValue();

                    sb.append("<movie>");
                    sb.append("<title>" + title + ": " + df.format(corr) + "</title>");
                    List<Flickr.ArtistPhoto> photos = flickr.getPhotos(title.replaceAll("[(0-9)+]", ""));
                    sb.append("<photos>");
                    for (Flickr.ArtistPhoto photo : photos) {
                        sb.append("<photo>" + photo.getSource() + "</photo>");
                    }
                    sb.append("</photos>");
                    sb.append("</movie>");
                    moviesAdded = true;
                } else {
                    break;
                }
                i++;
            }

            if (moviesAdded) {
                response.setContentType("text/xml");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().write("<movies>" + sb.toString() + "</movies>");
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        }
    }

    protected void forward(HttpServletRequest request,
            HttpServletResponse response, String path) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(path);
            rd.forward(request, response);
        } catch (Throwable tr) {
            if (log.isErrorEnabled()) {
                log.error("Caught Exception: " + tr.getMessage());
                log.debug("StackTrace:", tr);
            }
        }
    }
}
